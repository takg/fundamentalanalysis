package ConfigParams;

my $instance;
  
sub instance 
{
	unless (defined $instance) 
	{
		my $className = shift;
		my $self = 
		{
			_email => "a@b.com",
			_debug => "false",
			_pullStockListFromWebsite => "false",
			_bookValue => "",
			_debtassets => "",
			_dividend => "",
			_eps => "",
			_netgrowth => "",
			_price => "",
			_all => "",
			_dbName => "",
			_dbUser => "",
			_dbPassword => ""
		};	
	
		my $xpath = XML::XPath->new(filename=>"Config.xml");
		$self->{_email} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/mail");
		$self->{_debug} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/debug");
		
		$self->{_pullStockListFromWebsite} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/ticker");
		$self->{_all} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/all");
		
		if ( $self->{_all} eq "true")
		{
			$self->{_bookValue} = "true";
			$self->{_debtassets} = "true";
			$self->{_dividend} = "true";
			$self->{_eps} = "true";
			$self->{_netgrowth} = "true";
			$self->{_price} = "true";
		}
		else
		{
			$self->{_bookValue} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/bookvalue");
			$self->{_debtassets} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/debtassets");
			$self->{_dividend} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/dividend");
			$self->{_eps} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/eps");
			$self->{_netgrowth} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/netgrowth");
			$self->{_price} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/extractvalues/price");
		}
		
		$self->{_dbName} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/database/dbname");
		$self->{_dbUser} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/database/dbuser");
		$self->{_dbPassword} = $xpath->findvalue("/xml/application[\@name=\"ScreenScraping\"]/database/dbpassword");
	
		$instance = bless $self, $className;
    }

	return $instance;
}

sub getEmailIDs
{
	my ($self) = @_;
	
	return $self->{_email};	
}

sub isDebug
{
	my ($self) = @_;
	
	return $self->{_debug};	
}

# latest quotes. Otherwise we can set it to false 
# and download only once per day 
sub getStockListFromWebsite
{
	my($self) = @_;

	return $self->{_pullStockListFromWebsite};
}

sub getDBName
{
	my($self) = @_;

	return $self->{_dbName};
}

sub getDBPassword
{
	my($self) = @_;

	return $self->{_dbPassword};
}

sub getDBUser
{
	my($self) = @_;

	return $self->{_dbUser};
}

sub readBookValue
{
	my($self) = @_;

	return $self->{_bookValue};
}

sub readDebtAssets
{
	my($self) = @_;

	return $self->{_debtassets};
}

sub readDividend
{
	my($self) = @_;

	return $self->{_dividend};
}

sub readEPS
{
	my($self) = @_;

	return $self->{_eps};
}

sub readNetGrowth
{
	my($self) = @_;

	return $self->{_netgrowth};
}

sub readPrice
{
	my($self) = @_;

	return $self->{_price};
}

1;
