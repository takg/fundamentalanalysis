package Logger;

sub printLog
{
	my ($self,$arg) = @_;

	# if debug is set to true only then print
	if ( ConfigParams->instance()->isDebug() eq "true")
	{
		print "Logger :: $arg\n";		
	}
		
	return;
}

1;