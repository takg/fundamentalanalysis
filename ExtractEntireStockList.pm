package ExtractEntireStockList;

use LWP::Simple;
use HTML::TreeBuilder::XPath;
use XML::XPath;

# user defined packages
use Logger;
use ConfigParams;
use StockDatabaseHandler;

sub new
{
	my $className = shift;
	my $self = 
	{
		_stock => ()
	};
	
	bless $self, $className;
	return $self;
}

sub pullStockURLsFromWebsite
{	
	my ($self) = @_;

	# example $url = http://www.moneycontrol.com/india/stockmarket/pricechartquote/A
	# filename		 
	my $baseURL = "http://www.moneycontrol.com/india/stockmarket/pricechartquote/";
	my @letters = (A .. Z);
	
	foreach my $letter (@letters)
	{
		my $url = $baseURL.$letter;
		my $file = "download/pull_Stock_".$letter.".html";

		# if the url hasn't been loaded today download to a file
		if (!( -e $file))
		{
			Logger->printLog("loading $file");
			# reading url and saving it to file
			getstore($url,$file) or die 'cannot load url';
		}

		my $tree= HTML::TreeBuilder::XPath->new;
		$tree->parse_file($file);
		Logger->printLog("\n $file completed");

		#my @nodeset = $tree->findnodes('//table[@class="pcq_tbl MT10"]/tr[@bgcolor="#f6f6f6"]/td/a');
		my @nodeset = $tree->findnodes('//table[@class="pcq_tbl MT10"]/tr/td/a');		
		
		foreach my $node (@nodeset)
		{
			my $stockName = uc($node->getValue());
			$stockName =~ s/\s/_/g;
			$stockName =~ s/&/_/g;			
			my $attr = $node->findvalue('@href');
			${$self->{_stock}}{$stockName} = $attr;
		}

		# for the other set of rows
	}

	return;
}

sub saveToDB
{
	my ($self) = @_;	
	my $instance = new StockDatabaseHandler();
	my $dbh = $instance->getDBH();
	
	foreach $stockName (keys %{$self->{_stock}} )
	{
		my $url = ${$self->{_stock}}{$stockName};		
		if ( $stockName ne "" )
		{
			$stockName =~ s/'//g;
			# sector
			my @sections = split('/',$url);
			my $query = "select \"insertDataIntoTicker\"('$stockName','$url','@sections[5]')";
			my $rows = $dbh->do($query);
		}
	}
	
	$dbh->disconnect();
	
  	return;
}
1;