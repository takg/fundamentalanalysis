package ExtractStockDetails;

use LWP::Simple;
use HTML::TreeBuilder::XPath;
use XML::XPath;
use IO::Handle;

# multi threading modules
use threads;
use Thread::Semaphore;
use Thread::Queue;

# user defined packages
use Stock;
use Logger;
use ConfigParams;
use StockDatabaseHandler;

use constant MAX_NUMBER_THREADS => 3;

my $queryQueue : shared;
$queryQueue = Thread::Queue->new();

my $numberOfThreads : shared;
$numberOfThreads = 0;

sub new
{
	my $className = shift;
	my $self = 
	{
		_date => '2012-01-01',
		_numberOfThreads => 0
	};
	
	# get date
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
	$mon += 1; $year += 1900;
	$self->{_date} = "${year}-${mon}-${mday}";


	bless $self, $className;
	$self->print("Date : $self->{_date}");	

	return $self;
}

sub print
{
	my ($self,$val) = @_;
	print "\n".$val;

	return;
}

sub getVal
{
	my ($self,$tree,$url,$index) = @_;
	my $value = "";

	#if defined $tree->findnodes(url))[$index]
	$value = ($tree->findnodes(url))[$index]->findvalue(".");	
	
	return $value;
}

sub getStockDetails
{	
	my ($self, $name, $url1, $thread) = @_;
	
	eval
	{
		$self->print("Retrieving :: $name \n");		

		if ( ConfigParams->instance()->readPrice() eq "true" || 
			 ConfigParams->instance()->readBookValue() eq "true" || 
			 ConfigParams->instance()->readEPS() eq "true" ||
			 ConfigParams->instance()->readDividend() eq "true"
			 )
		{
			my $file1 = "download/price-${name}_1.html";
			Logger->printLog("\n Loading $file1");
			# reading url and saving it to file
			getstore($url1,$file1) or die 'cannot load url';
			
			my $tree = HTML::TreeBuilder::XPath->new;
			$tree->parse_file($file1);

			if ( ConfigParams->instance()->readPrice() eq "true")
			{
				# example $url = http://www.moneycontrol.com/india/stockpricequote/oildrillingandexploration/oilnaturalgascorporation/ONG
				# filename		
				# stock price
				my $price = $tree->findvalue('//span[@id="Bse_Prc_tick"]/strong');
				chomp($price);			
				# 52 week low
				my $low = ($tree->findnodes('//div[@class="PB3 gD_11"]/span[@class="PR5"]'))[1]->findvalue(".");	
				# 52 week high
				my $high = ($tree->findnodes('//div[@class="PB3 gD_11"]/span[@class="PL5"]'))[1]->findvalue(".");
				lock($queryQueue);
				my $queue = "select public.\"insertDataIntoPrice\"('$name',$price,$high,$low)";
				$queryQueue->enqueue($queue);
			}
					
			if (ConfigParams->instance()->readBookValue() eq "true")
			{
				# book value
				my $bookValue = ($tree->findnodes('//div[@class="PA7 brdb"]/div[@class="FR gD_12"]'))[2]->findvalue(".");
				$bookvalue = $bookvalue + 0.0;
				# send the data to db
				lock($queryQueue);
				my $queue = "select public.\"insertDataIntoBookValue\"('$name',$bookValue)";
				$queryQueue->enqueue($queue);
			}
			
			if (ConfigParams->instance()->readEPS() eq "true")
			{
				# EPS 
				my $eps = ($tree->findnodes('//div[@class="PA7 brdb"]/div[@class="FR gD_12"]'))[6]->findvalue(".");
				$eps = $eps + 0.0;
				# send the data to db
				lock($queryQueue);
				my $queue = "select public.\"insertDataIntoEPS\"('$name',$eps)";
				$queryQueue->enqueue($queue);
			}
			
			if (ConfigParams->instance()->readDividend() eq "true")
			{
				# Dividend
				my $dividend = ($tree->findnodes('//div[@class="PA7 brdb"]/div[@class="FR gD_12"]'))[3]->findvalue(".");
				$dividend =~ s/\%//g;
				$dividend = $dividend + 0.0;
				
				# Dividend Yield
				my $divyield = ($tree->findnodes('//div[@class="PA7 brdb"]/div[@class="FR gD_12"]'))[9]->findvalue(".");
				$divyield =~ s/\%//g;			
				$divyield = $divyield + 0.0;
				# Face Value
				my $facevalue = ($tree->findnodes('//div[@class="PA7 brdb"]/div[@class="FR gD_12"]'))[10]->findvalue(".");
				lock($queryQueue);
				my $queue = "select public.\"insertDataIntoDividend\"('$name',$divyield, $dividend, $facevalue)";
				$queryQueue->enqueue($queue);
			}

			$tree->delete;
		}		
		
		if (ConfigParams->instance()->readNetGrowth() eq "true")
		{
			# url for net profit
			#http://www.moneycontrol.com/financials/oilnaturalgascorporation/profit-loss/ONG#ONG
			my @splits = split('/',$url1);
			$url2 = $splits[0].'/'.$splits[1].'/'.$splits[2].'/financials/'.$splits[6].'/profit-loss/'.$splits[7].'#'.$splits[7];
			# filename		 
			my $file2 = "download/price-${name}_2.html";
			my $tree2 = HTML::TreeBuilder::XPath->new;

			# if the url hasn't been loaded today download to a file
			Logger->printLog("\nloading $file2");
			# reading url and saving it to file
			getstore($url2,$file2) or die 'cannot load url';
			
			$tree2->parse_file($file2);
			# net profit year on year
			my $year0 = ($tree2->findnodes('//td[text()="Reported Net Profit"]/../td[@align="right"]|//td[text()="Net Profit for the Year"]/../td[@align="right"]'))[0]->findvalue('.');
			my $year1 = ($tree2->findnodes('//td[text()="Reported Net Profit"]/../td[@align="right"]|//td[text()="Net Profit for the Year"]/../td[@align="right"]'))[1]->findvalue('.');
			my $year2 = ($tree2->findnodes('//td[text()="Reported Net Profit"]/../td[@align="right"]|//td[text()="Net Profit for the Year"]/../td[@align="right"]'))[2]->findvalue('.');
			my $year3 = ($tree2->findnodes('//td[text()="Reported Net Profit"]/../td[@align="right"]|//td[text()="Net Profit for the Year"]/../td[@align="right"]'))[3]->findvalue('.');
			my $year4 = ($tree2->findnodes('//td[text()="Reported Net Profit"]/../td[@align="right"]|//td[text()="Net Profit for the Year"]/../td[@align="right"]'))[4]->findvalue('.');
			
			# removing commas
			$year0 =~ s/,//g;
			$year1 =~ s/,//g;
			$year2 =~ s/,//g;
			$year3 =~ s/,//g;
			$year4 =~ s/,//g;
			
			# send the data to db
			lock($queryQueue);
			my $query = "select public.\"insertDataIntoNetGrowthYoY\"('$name', $year0, $year1, $year2, $year3, $year4);";
			$queryQueue->enqueue($query);
			
			$tree2->delete;
		}
		
		# http://www.moneycontrol.com/financials/oilnaturalgascorporation/balance-sheet/ONG#ONG
		# get the total debt and equity
		# url for net profit
		my $url3 = $splits[0].'/'.$splits[1].'/'.$splits[2].'/financials/'.$splits[6].'/balance-sheet/'.$splits[7].'#'.$splits[7];
		
		if (ConfigParams->instance()->readDebtAssets() eq "true")
		{
			# filename		 
			my $file3 = "download/price-${name}_3.html";

			Logger->printLog("\nloading $file3");
			# reading url and saving it to file
			getstore($url3,$file3) or die 'cannot load url';
			
			my $tree3 = HTML::TreeBuilder::XPath->new;
			$tree3->parse_file($file3);
			# total debt
			my $debt = ($tree3->findnodes('//td[text()="Total Debt"]/../td[@align="right"]'))[0]->findvalue('.');
			$debt =~ s/,//g;
			$debt = $debt +0.0;
			my $totalassets = ($tree3->findnodes('//td[text()="Total Assets"]/../td[@align="right"]'))[0]->findvalue('.');
			$totalassets =~ s/,//g;
			$tree3->delete;
			$totalassets = $totalassets + 0.0;
		
			# send the data to db
			lock($queryQueue);
			my $queue = "select public.\"insertDataIntoDebtAssets\"('$name', $debt, $totalassets)";
			$queryQueue->enqueue($queue);
		}
		
		$self->print("Completed :: $name \n");
		1;
	} or do
	{
		$self->print("Error caught for - $name :: $@");
		my $query ="select \"excludeStock\"('$name')";
		lock($queryQueue);
		$queryQueue->enqueue($query);
	};
	
	lock($numberOfThreads);
	$numberOfThreads--;
	return;
}

sub getStockNames
{
	my ($self) = @_;	

	my $instance = new StockDatabaseHandler();
	my $dbh = $instance->getDBH();
	my $query = 'SELECT "StockName","URL","Sector" FROM public."Ticker_V"';
	my $sth = $dbh->prepare($query);
	$sth->execute();

	my @records = ();
	
	while( my @data = $sth->fetchrow_array()  )
	{
		my @array = ($data[0], $data[1] );
		push(@records, [@array]);
	}
	
	$sth->finish();
	$dbh->disconnect();
	
	my $count = 0;
	foreach my $row (@records)
	{
		while(1)
		{
			if ( $numberOfThreads < MAX_NUMBER_THREADS )
			{
				my $thread = threads->create(\&getStockDetails, $self, @$row[0], @$row[1], $numberOfThreads);
				$thread->detach();
				lock($numberOfThreads);
				$numberOfThreads++;
				$count++;
				if ( $count %10 == 0 )
				{
					print ("\n $count started. ");
				}
				last;
			}
			else
			{
				# waiting for the running thread to complete
				# NOTE: Why this way --- Seems like Perl does not release memory to OS, 
				# causing the daemon process to go out of memory!
				sleep 1;
			}
		}
	}

	lock($queryQueue);
	$queryQueue->enqueue("EOD"); # end of data
	$self->print("Completed reading file.");
	return;
}

sub retrieveStockDetails
{
	my ($self) = @_;	
	
	my $threadWriteToFile = threads->create(\&saveToDB, $self);
	getStockNames($self);	
	$threadWriteToFile->join();	
	
  	return;
}

sub saveToDB
{
	my ($self) = @_;	
	my $instance = new StockDatabaseHandler();
	my $dbh = $instance->getDBH();
	my $query;
	
	while ($query ne "EOD")
	{
		if ($queryQueue->pending())
		{
			lock($queryQueue);
			$query = $queryQueue->dequeue();
			if ($query ne "EOD")
			{
				$dbh->do($query);
			}
		}
		else
		{
			sleep 2;
		}
	}
	
	$dbh->disconnect();
	
	return;
}

1;