use strict;
use warnings;

# user defined packages
use ExtractStockDetails;
use ExtractEntireStockList;
use Logger;

my $start = time();

if (ConfigParams->instance()->getStockListFromWebsite() eq "true")
{
	my $pullStockList = new ExtractEntireStockList();
	$pullStockList->pullStockURLsFromWebsite();
	$pullStockList->saveToDB();
}
else
{
	my $screenscraping = new ExtractStockDetails();
	$screenscraping->retrieveStockDetails();
}

my $end = time();
print "\nTime taken :: " , ($end - $start), " seconds.";

