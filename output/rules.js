function init(id)
{
	calculateScore(id);
	sort(id,24);
}

function calculateScore(id)
{
	var list=document.getElementById("algoScoreCalc");
	var value = list.options[list.selectedIndex].text;
	
	if (value == 'Scale-Based')
	{
		calculateScoreOnScale(id)
	}
	else if (value == 'Data-Based')
	{
		calculateScoreOnAbsoluteScale(id);
	}
}

// this function shall calculate the overall score and
// the score is used as a threshold parameter for deciding
// a good stock vs bad stock.
function calculateScoreOnScale(id)
{
	//alert('calculateScoreOnScale');
	// 1. get all the rows
	// 2. for each row calculate the score
	// 3. populate the score

    //gets table
	var oTable = document.getElementById(id);
    //gets rows of table
    var rowLength = oTable.rows.length;

	// the max score possible
	var maxScore = 0;
	
	// score
	var excellent = 20;
	var good = 10;
	var average = 0;
	var bad = -10;
	var worse = -20;
	
	// scale
	var scalePB = 1.0;
	var scalePE = 1.0;
	var scaleDebt = 1.5;
	var scaleCMP = 2.0;
	var scaleDiv = 1.0;
	var scaleYear1 = 4.0;
	var scaleYear2 = 3.0;
	var scaleYear3 = 2.0;
	var scaleYear4 = 1.0;
	
	// indexes
	var indexPB = 6;
	var indexPE = 7;
	var indexDiv = 21;
	var indexCMP = 2;
	var indexYearHigh = 3;
	var indexTotalAssets = 19;
	var indexDebt = 18;
	var indexYoY1 = 9;
	var indexYoY2 = 10;
	var indexYoY3 = 11;
	var indexYoY4 = 12;
	var indexScore = 24;
	var indexMaxScore = 25;
	

	//loops through rows
    for (i = 1; i < rowLength; i++)
	{
		var score = 0;
		var maxScore = 0;
		var oCells = oTable.rows.item(i).cells;
		
		var pbratio = oCells.item(indexPB).innerHTML;		
		maxScore += scalePB * excellent;
		if (pbratio < -1.0)
		{
			score += scalePB * worse;
		}
		else if (pbratio < 0.0)
		{
			score += scalePB * bad;
		}
		else if (pbratio < 2.0)
		{
			score += scalePB * excellent;
		}
		else if (pbratio < 2.5)
		{
			score += scalePB * good;
		}
		else
		{
			score += scalePB * average;
		}
		
		// P/E Ratio
		var peratio = oCells.item(indexPE).innerHTML;		
		maxScore += scalePE * excellent;
		if (peratio < -5)
		{
			score += scalePE * worse;
		}
		else if (peratio < 0)
		{
			score += scalePE * bad;
		}
		else if (peratio < 15)
		{
			score += scalePE * excellent;
		}
		else if (peratio < 20)
		{
			score += scalePE * good;
		}
		else
		{
			score += scalePE * average;
		}

		// Dividend Yield
		var divyield = oCells.item(indexPE).innerHTML;
		maxScore += scaleDiv * excellent;
		if (divyield < 2)
		{
			score += scaleDiv * average;
		}
		else if (divyield < 4)
		{
			score += scaleDiv * good;
		}
		else
		{
			score += scaleDiv * excellent;
		}

		// CMP comparision with 52 wk high/low
		var price = oCells.item(indexCMP).innerHTML;
		var yearHigh = oCells.item(indexYearHigh).innerHTML;
		var value = price / yearHigh * 100;
		maxScore += scaleCMP * excellent;
		if ( value != 0)
		{
			if (value < 60)
			{
				score += scaleCMP * excellent;
			}
			else if (value < 75)
			{
				score += scaleCMP * good;
			}
			else if (value < 85)
			{
				score += scaleCMP * average;
			}
			else
			{
				score += scaleCMP * bad;
			}
		}

		// Debt vs Total Assets
		maxScore += scaleDebt * excellent;
		var debt = oCells.item(indexDebt).innerHTML;
		var totalassets = oCells.item(indexTotalAssets).innerHTML;
		value = debt / totalassets * 100;

		if (value < 10)
		{
			score += scaleDebt * excellent;
		}
		else if (value < 40)
		{
			score += scaleDebt * good;
		}
		else if (value < 100)
		{
			score += scaleDebt * average;
		}
		else
		{
			score += scaleDebt * bad;
		}

		// yearly growth 1
		value = oCells.item(indexYoY1).innerHTML;		
		maxScore += scaleYear1 * excellent;
		if (value < -10)
		{
			score += scaleYear1 * worse;
		}
		if (value < 0)
		{
			score += scaleYear1 * bad;
		}
		else if (value < 10)
		{
			score += scaleYear1 * average;
		}
		else if (value < 25)
		{
			score += scaleYear1 * good;
		}
		else
		{
			score += scaleYear1 * excellent;
		}

		// yearly growth 2
		value = oCells.item(indexYoY2).innerHTML;		
		maxScore += scaleYear2 * excellent;
		if (value < -10)
		{
			score += scaleYear2 * worse;
		}
		if (value < 0)
		{
			score += scaleYear2 * bad;
		}
		else if (value < 10)
		{
			score += scaleYear2 * average;
		}
		else if (value < 25)
		{
			score += scaleYear2 * good;
		}
		else
		{
			score += scaleYear2 * excellent;
		}

		// yearly  3
		value = oCells.item(indexYoY3).innerHTML;		
		maxScore += scaleYear3 * excellent;
		if (value < -10)
		{
			score += scaleYear3 * worse;
		}
		if (value < 0)
		{
			score += scaleYear3 * bad;
		}
		else if (value < 10)
		{
			score += scaleYear3 * average;
		}
		else if (value < 25)
		{
			score += scaleYear3 * good;
		}
		else
		{
			score += scaleYear3 * excellent;
		}

		// yearly growth 4
		value = oCells.item(indexYoY4).innerHTML;		
		maxScore += scaleYear4 * excellent;
		if (value < -10)
		{
			score += scaleYear4 * worse;
		}
		if (value < 0)
		{
			score += scaleYear4 * bad;
		}
		else if (value < 10)
		{
			score += scaleYear4 * average;
		}
		else if (value < 25)
		{
			score += scaleYear4 * good;
		}
		else
		{
			score += scaleYear4 * excellent;
		}
		
		oCells.item(indexScore).innerHTML = score;
		oCells.item(indexMaxScore).innerHTML = maxScore;
    }
	
	//alert('calculateScoreOnScale');
}

// this function shall calculate the overall score and
// the score is used as a threshold parameter for deciding
// a good stock vs bad stock.
function calculateScoreOnAbsoluteScale(id)
{
	//alert('calculateScoreOnAbsoluteScale');
	// 1. get all the rows
	// 2. for each row calculate the score
	// 3. populate the score

    //gets table
	var oTable = document.getElementById(id);
    //gets rows of table
    var rowLength = oTable.rows.length;

	// the max score possible
	var maxScore = 0;
	
	// scale
	var scalePB = 1.0;
	var scalePE = 1.0;
	var scaleDebt = 1.5;
	var scaleCMP = 2.0;
	var scaleDiv = 1.0;
	var scaleYear1 = 4.0;
	var scaleYear2 = 3.0;
	var scaleYear3 = 2.0;
	var scaleYear4 = 1.0;
	
	// indexes
	var indexPB = 6;
	var indexPE = 7;
	var indexDiv = 21;
	var indexCMP = 2;
	var indexYearHigh = 3;
	var indexTotalAssets = 19;
	var indexDebt = 18;
	var indexYoY1 = 9;
	var indexYoY2 = 10;
	var indexYoY3 = 11;
	var indexYoY4 = 12;
	var indexScore = 24;
	var indexMaxScore = 25;

	//loops through rows
    for (i = 1; i < rowLength; i++)
	{
		var score = 0;
		var maxScore = 0;
		var oCells = oTable.rows.item(i).cells;
		var value = 1.0;
				
		var pbratio = oCells.item(indexPB).innerHTML;		
		if (pbratio != 0)
		{
			score += scalePB * 2.0 / pbratio * 100 ;
			maxScore += scalePB * 2.0 / 2.0 * 100; // best case p/b = 2.0
		}		
		
		// P/E Ratio
		var peratio = oCells.item(indexPE).innerHTML;
		if (peratio != 0 && peratio != "-")
		{
			score += scalePE * 20.0 / peratio * 100;
			maxScore += scalePE * 20.0 / 10 * 100; // best case p/e = 10
		}
		
		// Dividend Yield
		var divyield = oCells.item(indexPE).innerHTML;
		score += scaleDiv * divyield * 100;
		maxScore += scaleDiv * 5 * 100; // best div = 5% p.a.

		// CMP comparision with 52 wk high/low
		var price = oCells.item(indexCMP).innerHTML;
		var yearHigh = oCells.item(indexYearHigh).innerHTML;
		var value = price / yearHigh * 100;
		if ( value != 0)
		{
			score += scaleCMP * 100.0 / value * 100;
			maxScore += scaleCMP * 100.0 / 50 * 100; // best case CMP = 50% of Year high
		}

		// Debt vs Total Assets
		var debt = oCells.item(indexDebt).innerHTML;
		if (debt != "-")
		{
			var totalassets = oCells.item(indexTotalAssets).innerHTML;
			value = debt / totalassets * 100;
			if (value == 0)
			{
				value = 1;
			}
			score += scaleDebt * 1.0 / value;
			maxScore += scaleDebt * 1.0 / 0.1; // best case debt = 0
		}
		
		// yearly growth 1
		value = oCells.item(indexYoY1).innerHTML;		
		score += scaleYear1 * value;
		maxScore += scaleYear1 * 25;
		// yearly growth 2
		value = oCells.item(indexYoY2).innerHTML;		
		score += scaleYear2 * value;
		maxScore += scaleYear2 * 25;
		// yearly  3
		value = oCells.item(indexYoY3).innerHTML;		
		score += scaleYear3 * value;
		maxScore += scaleYear3 * 25;
		// yearly growth 4
		value = oCells.item(indexYoY4).innerHTML;		
		score += scaleYear4 * value;
		maxScore += scaleYear4 * 25;
		
		oCells.item(indexScore).innerHTML = Math.round(score);
		oCells.item(indexMaxScore).innerHTML = maxScore;
    }
	
	//alert('calculateScoreOnAbsoluteScale');
}

function setAlternateRowsBGColor(id)
{ 
	if(document.getElementsByTagName)
	{
		var table = document.getElementById(id);
		var rows = table.getElementsByTagName("tr");
		var flag = 0;
		var indexSlNo = 0;

		for(i = 1; i < rows.length; i++)
		{
			//manipulate rows 			
			if (rows[i].style.display != "none")
			{
				flag++;
				table.rows.item(i).cells.item(indexSlNo).innerHTML = flag;

				if(flag % 2 == 0)
				{ 
					rows[i].className = "even"; 
				}
				else
				{ 
					rows[i].className = "odd";
				}				
			}
		} 
	}
	
	//alert('setAlternateRowsBGColor');
}

function hideRows(id)
{ 
	if(document.getElementsByTagName)
	{
		var table = document.getElementById(id);
		var rows = table.getElementsByTagName("tr");

		for(i = 1; i < rows.length; i++)
		{
			//manipulate rows 
			rows[i].style.display = "none"; 
		} 
	}
	
	//alert('hideRows');
}

function showRows(id)
{ 
	if(document.getElementsByTagName)
	{
		var table = document.getElementById(id);
		var rows = table.getElementsByTagName("tr");

		for(i = 1; i < rows.length; i++)
		{
			//manipulate rows 
			rows[i].style.display = ""; 
		} 
	}
	
	//alert('showRows');
}

function sort(id, colIndex)
{
	//hideRows(id);
	
	if(document.getElementsByTagName)
	{
		var table = document.getElementById(id);	
		var rows = table.getElementsByTagName("tr");

		//alert(rows.length);
		// creates a JS array of DOM elements		
		for (var i = 1; i < rows.length; i++) 
		{
			var index = 1;
			var xData = Number(rows[index].cells.item(colIndex).innerHTML);
			var lastIndex = rows.length;// - (i);
			
			for (var j = 1; j < rows.length - (i - 1); j++) 
			{
				var yData = Number(rows[j].cells.item(colIndex).innerHTML);
				
				if ( yData > xData )
				{
					index = j;
					xData = yData;
				}
			}
			
			// now swap the items index with lastIndex
			//alert('swapping with ' +index +' and ' + lastIndex + ' ' + xData + ' ' + yData);
			
			if (index != lastIndex)
			{
				table.insertBefore(rows[index], rows[lastIndex]);
			}
		}
	}
	
	//alert('sort');
	//showRows(id);
	setAlternateRowsBGColor(id);
}

// filter 
function filter(id)
{
	showRows(id);
	var colIndex = 19; // total assets
	var list=document.getElementById("capFilter");
	var value = list.options[list.selectedIndex].text;
	
	if(document.getElementsByTagName)
	{
		var table = document.getElementById(id);
		var rows = table.getElementsByTagName("tr");

		for(i = 1; i < rows.length; i++)
		{
			//manipulate rows 
			var data = Number(rows[i].cells.item(colIndex).innerHTML);
			// large cap
			if ((value == 'LargeCap') && (data < 5000))
			{
				rows[i].style.display = "none"; 
			}
			// mid cap
			else if ((value == 'MidCap') && ((data < 500) || (data > 5000)))
			{
				rows[i].style.display = "none"; 
			}
			// small cap
			else if ((value == 'SmallCap') && (data > 500))
			{
				rows[i].style.display = "none"; 
			}
		} 
	}
	
	setAlternateRowsBGColor(id);
}

