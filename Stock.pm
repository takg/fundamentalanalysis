# This file has logic for going to www.moneycontrol.com
# and retrieving the below stock details
# 1. Share Price
# 2. Book Value
# 3. Calculate and return the P/B ratio
# 4. EPS
# 5. P/E ratio
# 6. YoY growth for the last max years
# 7. QoQ growth for the last max years
# 8. Debt
# 9. D/E ratio
# 10. Total Score
# 11. Dividend

package Stock;

use Logger;
use ConfigParams;

sub new
{
	my $className = shift;
	my $self =
	{
		_name => "",
		_price => 0,
		_bookValue => 0,
		_EPS => 0,
		_yearlyGrowth => (),
		_debt => 0,
		_totalassets => 0,
		_score => 0,
		_scorePer => 0,
		_dividend => 0,
		_dividendYield => 0,
		_faceValue => 0,
		_sector => 0,
		_yearHigh => 0,
		_yearLow => 0,
		_netProfitYoY => ()
	};

	bless $self,$className;
	return $self;
}

sub setName()
{
	my ($self,$arg1) = @_;
	$self->{_name} = $arg1;
	return;
}

sub setPrice()
{
	my ($self,$arg1) = @_;
	$self->{_price} = $arg1;
	return;
}

sub setBookValue()
{
	my ($self,$arg1) = @_;
	$self->{_bookValue} = $arg1;
	return;
}

sub setEPS()
{
	my ($self,$arg1) = @_;
	$self->{_EPS} = $arg1;
	return;
}

sub setDividend()
{
	my ($self,$arg1) = @_;
	$self->{_dividend} = $arg1;
	return;
}

sub setDividendYield()
{
	my ($self,$arg1) = @_;
	$self->{_dividendYield} = $arg1;
	return;
}

sub setFaceValue()
{
	my ($self,$arg1) = @_;
	$self->{_faceValue} = $arg1;
	return;
}

sub setSector()
{
	my ($self,$arg1) = @_;
	$self->{_sector} = $arg1;
	return;
}

sub setYearHigh()
{
	my ($self,$arg1) = @_;
	$self->{_yearHigh} = $arg1;
	return;
}

sub setYearLow()
{
	my ($self,$arg1) = @_;
	$self->{_yearLow} = $arg1;
	return;
}

sub setDebt()
{
	my ($self,$arg1) = @_;
	$self->{_debt} = $arg1;
	return;
}

sub setTotalAssets()
{
	my ($self,$arg1) = @_;
	$self->{_totalassets} = $arg1;
	return;
}

sub setNetProfitYoY()
{
	my ($self,@arg1) = @_;
	@{$self->{_netProfitYoY}} = @arg1;

	my $size = $#arg1;
	my $loop = 0;
	my @percentage = ();

	while($loop < $size )
	{
		my $final = @arg1[$loop];
		my $initial = @arg1[$loop + 1 ];
        my $per = ($final - $initial)/$initial*100;

		if ($initial < 0)
		{
            $per = ($per)*(-1.0);
        }

		$per = sprintf("%.2f",$per);
		@percentage = (@percentage,$per);
		$loop++;
	}

	@{$self->{_yearlyGrowth}} = @percentage;
	return;
}

sub printLine
{
	my ($arg1,$arg2) = @_;
	Logger->printLog("$arg1 : <$arg2>");
	return;
}

sub print
{
	my ($self) = @_;
	Logger->printLog("");
	printLine("Stock name",$self->{_name});
	printLine("Price",$self->{_price});
	printLine("Book value",$self->{_bookValue});
	printLine("EPS",$self->{_EPS});
	printLine("Yearly growth",@{$self->{_yearlyGrowth}});
	printLine("Debt",$self->{_debt});
	printLine("Total assets ",$self->{_totalassets});
	printLine("Dividend",$self->{_dividend});
	printLine("Dividend yield",$self->{_dividendYield});
	printLine("Face Value",$self->{_faceValue});
	printLine("Sector",$self->{_sector});
	printLine("52 week High",$self->{_yearHigh});
	printLine("52 week Low",$self->{_yearLow});
	printLine("Net year profit",@{$self->{_netProfitYoY}});
	printLine("Total score(%)",$self->{_scorePer});
	printLine("Total score",$self->{_score});

	return;
}

sub toXML()
{
	my ($self) = @_;
	my $xml = "";

	$xml .= "\n<stock ";
	$xml .= "name=\"$self->{_name}\" ";
	$xml .= "price=\"$self->{_price}\" ";
	$xml .= "bookvalue=\"$self->{_bookValue}\" ";
	$xml .= "eps=\"$self->{_EPS}\" ";

	$count = 1;
	foreach my $val (@{$self->{_yearlyGrowth}})
	{
		$xml .= "yearlygrowth$count=\"$val\" ";
		$count++;
	}

	$count = 1;
	foreach my $val (@{$self->{_netProfitYoY}})
	{
		$xml .= "netprofityoy$count=\"$val\" ";
		$count++;
	}
	$xml .= "debt=\"$self->{_debt}\" ";
	$xml .= "totalassets=\"$self->{_totalassets}\" ";
	$xml .= "dividend=\"$self->{_dividend}\" ";
	$xml .= "dividendyield=\"$self->{_dividendYield}\" ";
	$xml .= "facevalue=\"$self->{_faceValue}\" ";
	$xml .= "sector=\"$self->{_sector}\" ";
	$xml .= "totalscore=\"$self->{_score}\" ";
	$xml .= "totalscoreper=\"$self->{_scorePer}\" ";
	$xml .= "yearhigh=\"$self->{_yearHigh}\" ";
	$xml .= "yearlow=\"$self->{_yearLow}\" ";
	$xml .= "/>";

	return $xml;
}

sub toHTML()
{
	my ($self) = @_;
	my $html = "";

	$html .= "<td class='slno'></td>";
	$html .= "<td class='name'>$self->{_name}</td>";
	$html .= "<td class='price'>$self->{_price}</td>";
	$html .= "<td class='yearhigh'>$self->{_yearHigh}</td>";
	$html .= "<td class='yearlow'>$self->{_yearLow}</td>";
	$html .= "<td class='bookvalue'>$self->{_bookValue}</td>";
	$html .= "<td class='eps'>$self->{_EPS}</td>";
	
	$count = 1;
	foreach my $val (@{$self->{_yearlyGrowth}})
	{
		$html .= "<td class='yearlygrowth$count'>$val</td>";
		$count++;
	}

	$count = 1;
	foreach my $val (@{$self->{_netProfitYoY}})
	{
		$html .= "<td class='netprofityoy$count'>$val</td>";
		$count++;
	}

	$html .= "<td class='debt'>$self->{_debt}</td>";
	$html .= "<td class='totalassets'>$self->{_totalassets}</td>";
	$html .= "<td class='dividend'>$self->{_dividend}</td>";
	$html .= "<td class='dividendyield'>$self->{_dividendYield}</td>";
	$html .= "<td class='facevalue'>$self->{_faceValue}</td>";
	$html .= "<td class='sector'>$self->{_sector}</td>";
	$html .= "<td class='score'>$self->{_score}</td>";
	$html .= "<td class='scorepercentage'>$self->{_scorePer}</td>";

	$count++;

	return $html;
}
# package needs to return true value
1;
