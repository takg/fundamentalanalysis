package StockDatabaseHandler;

# this module shall interact with PostGRE database

use DBI;
use ConfigParams;

sub new
{
	my $className = shift;
	my $self = 
	{
		_dbh => ''
	};
	
	bless $self, $className;

	return $self;
}

sub getDBH
{
	my ($self) = @_;
	
	$self->{_dbh} = DBI->connect("DBI:Pg:dbname=".ConfigParams->instance()->getDBName().";host=localhost",
							ConfigParams->instance()->getDBUser(),
							"albert"
							);
	#print ("\n tesing <".ConfigParams->instance()->getDBPassword().">");
	return $self->{_dbh};	
}

sub runQuery
{
	my ($self) = @_;

	my $query = 'select "insertDataIntoTicker"(\'SBI\',\'http://\',\'banking\')';
	my $rows = $self->{_dbh}->do($query);
	print "\n $query -> $rows affected";
	$self->{_dbh}->commit();
}

1;